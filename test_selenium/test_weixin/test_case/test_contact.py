import pytest

from test_selenium.test_weixin.PO.contactPage import ContactPage


class TestContact:
    @pytest.mark.parametrize("name", ["UI自动化测试3"])
    @pytest.mark.parametrize("acc_id", ["113"])
    @pytest.mark.parametrize("phone_no", ["13512737678"])
    def test_contact(self,name,acc_id,phone_no):
        contact_page = ContactPage()

        assert "UI自动化测试3" in contact_page.go_to_addmemberPage().addMember(name,acc_id,phone_no).getContactList()