import pytest

from test_selenium.test_weixin.PO.mainPage import MainPage


class TestAddMember:
    @pytest.mark.parametrize("name", ["UI自动化测试1"])
    @pytest.mark.parametrize("acc_id", ["110"])
    @pytest.mark.parametrize("phone_no", ["13512737675"])
    # 验证成功添加
    def test_add_member(self, name, acc_id, phone_no):
        main_page = MainPage()
        '''
            1.跳转至添加成员页面
            2.输入相关信息后，点击保存跳转至成员列表页面
            3.在成员列表页面校验是否添加正确
        '''

        assert name in main_page.go_to_addmemberPage().addMember(name).getContactList()

    # 验证重复添加后添加不成功
    @pytest.mark.parametrize("name", ["UI自动化测试2"])
    @pytest.mark.parametrize("acc_id", ["111"])
    @pytest.mark.parametrize("phone_no", ["13512737673"])
    def test_add_member_fail(self, name, acc_id, phone_no):
        main_page = MainPage()
        '''
            1.跳转至添加成员页面
            2.输入相关信息后，点击保存跳转至成员列表页面
            3.在成员列表页面校验是否添加正确
        '''

        assert "UI自动化测试" in main_page.go_to_addmemberPage().addMember(name, acc_id, phone_no).getContactList()
