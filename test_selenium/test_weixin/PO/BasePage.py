from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver


class BasePage:
    base_url = ""

    def __init__(self, base_driver=None):
        if base_driver is None:
            # 复用remote
            # chrome_arg = webdriver.ChromeOptions()
            # chrome_arg.debugger_address = '127.0.0.1:9229'
            # self.driver = webdriver.Chrome(options=chrome_arg)
            option = Options()
            option.debugger_address = '127.0.0.1:9229'
            self.driver = webdriver.Chrome(options=option)
            # 打开首页
            self.driver.get(self.base_url)
            print(self.driver.get_cookies())
            self.driver.implicitly_wait(3)
        else:
            self.driver: WebDriver = base_driver

    # 查找页面元素
    def find(self, by, locator=None):
        if locator is None:
            return self.driver.find_element(*by)
        else:
            return self.driver.find_element(by=by, value=locator)

    def finds(self, by, locator=None):
        if locator is None:
            return self.driver.find_elements(*by)
        else:
            return self.driver.find_elements(by=by, value=locator)
