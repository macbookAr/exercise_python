from selenium.webdriver.common.by import By
from test_selenium.test_weixin.PO.BasePage import BasePage
from test_selenium.test_weixin.PO.addMemberPage import AddMemberPage


class ContactPage(BasePage):
    base_url = "https://work.weixin.qq.com/wework_admin/frame#contacts"
    button_tab = (By.ID, "menu_contacts")
    button_add_member = (By.CSS_SELECTOR, ".js_add_member")

    # 跳转至添加用户也没按
    def go_to_addmemberPage(self):
        # 点击进入到通讯录
        self.find(*self.button_tab).click()
        # 点击跳转至 添加通讯录页面
        self.find(*self.button_add_member).click()
        return AddMemberPage(self.driver)

    def getContactList(self):
        # 获取用户列表
        ele = self.finds(By.CSS_SELECTOR, ".member_colRight_memberTable_td:nth-child(2)")
        return [name.text for name in ele]
