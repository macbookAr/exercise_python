from selenium.webdriver.common.by import By
from test_selenium.test_weixin.PO.BasePage import BasePage
from test_selenium.test_weixin.PO.addMemberPage import AddMemberPage
from test_selenium.test_weixin.PO.contactPage import ContactPage


class MainPage(BasePage):
    base_url = "https://work.weixin.qq.com/wework_admin/frame#index"
    # 跳转至通讯录页面
    def go_to_contactPage(self):

        return ContactPage()

    # 点击跳转至添加用户页面
    def go_to_addmemberPage(self):
        # 点击添加成员
        self.driver.find_element(By.CSS_SELECTOR,".ww_indexImg_AddMember").click()
        # self.driver.find_element_by_class_name("ww_indexImg ww_indexImg_AddMember").click()
        return AddMemberPage(self.driver)
