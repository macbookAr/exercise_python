from selenium.webdriver.common.by import By
from test_selenium.test_weixin.PO.BasePage import BasePage


class AddMemberPage(BasePage):
    #抽离元素
    username_ele = (By.ID, "username")
    accid_ele = (By.ID, "memberAdd_acctid")
    phone_ele = (By.ID, "memberAdd_phone")
    button_ele = (By.CSS_SELECTOR, ".js_btn_save")

    # 进行添加用户操作
    def addMember(self,name,acc_id,phone_no):
        # 解决循环导入
        from test_selenium.test_weixin.PO.contactPage import ContactPage
        # # 输入姓名 解包元祖
        self.find(*self.username_ele).send_keys(name)
        # # 输入账号
        self.find(*self.accid_ele).send_keys(acc_id)
        # # 输入手机号
        self.find(*self.phone_ele).send_keys(phone_no)
        # # 点击 保存
        self.find(*self.button_ele).click()

        return ContactPage(self.driver)
