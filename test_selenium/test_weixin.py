from time import sleep

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By


class TestDemo0():
    def setup_method(self, method):
        # option = Options()
        # option.debugger_address = '127.0.0.1:9229'
        # self.driver = webdriver.Chrome(options=option)
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)
        # option = Options()
        # option.debugger_address = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome()
        # self.driver.implicitly_wait(5)

    def teardown_nethod(self):
        self.driver.quit()

    # def test_cookie(self):
    #     self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
    #     # self.driver.set_window_size(1536,960)
    #     # self.driver.find_element(By.LINK_TEXT,"所有分类").click()
    #     # categoryele =  self.driver.find_element(By.LINK_TEXT,"所有分类")
    #     # assert  'active'== categoryele.get_attribute("class")
    #     # 获取当前页面的cookie
    #     cookies = self.driver.get_cookies()
    #     print(cookies)
    # cookies = [
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.cs_ind', 'path': '/', 'secure': False,
    #      'value': ''},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False,
    #      'value': '1688850056965497'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False,
    #      'value': '1688850056965497'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False,
    #      'value': '1'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False,
    #      'value': 'a3967646'},
    #     {'domain': '.qq.com', 'expiry': 1627195426.900, 'httpOnly': False, 'name': '_gat', 'path': '/',
    #      'secure': False, 'value': '1'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': False,
    #      'value': '9214628972566511'},
    #     {'domain': '.qq.com', 'expiry': 1690267383, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False,
    #      'value': 'GA1.2.1879260742.1627190232'},
    #     {'domain': '.work.weixin.qq.com', 'expiry': 1629787386, 'httpOnly': False, 'name': 'wwrtx.i18n_lan',
    #      'path': '/', 'secure': False, 'value': 'zh'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqunionid', 'path': '/',
    #      'secure': False, 'value': ''},
    #     {'domain': '.qq.com', 'expiry': 1652018377, 'httpOnly': False, 'name': 'LW_uid', 'path': '/',
    #      'secure': False, 'value': '31w6N2D0T4b822R3j7y7s6Q436'},
    #     {'domain': '.qq.com', 'expiry': 1652018377, 'httpOnly': False, 'name': 'LW_sid', 'path': '/',
    #      'secure': False, 'value': 'I10682U0q4x872d3c7R7d63425'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'tmeLoginType', 'path': '/',
    #      'secure': False, 'value': '2'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqopenid', 'path': '/',
    #      'secure': False, 'value': '961481CB923D6B51200EF0615698F47E'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxopenid', 'path': '/',
    #      'secure': False, 'value': ''},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxrefresh_token', 'path': '/',
    #      'secure': False, 'value': ''},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_access_token_expiresAt',
    #      'path': '/', 'secure': False, 'value': '1627915053'},
    #     {'domain': '.qq.com', 'expiry': 2147483646, 'httpOnly': False, 'name': 'RK', 'path': '/', 'secure': False,
    #      'value': 'AJaYwC27SJ'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqrefresh_token', 'path': '/',
    #      'secure': False, 'value': '62EBF2E9CA827F301E04CC0CEBDFFA21'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'euin', 'path': '/', 'secure': False,
    #      'value': 'oK4qow-sNK-*'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqaccess_token', 'path': '/',
    #      'secure': False, 'value': 'EFA3150DA9CE7CD92338DC59EEC3A38D'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False,
    #      'value': 'jeb96G-C8QJfsp_3K0_D-v5cIteSs9jMo0e23rs4C2xYaSPbrhpKRRNMpa_dGCKS'},
    #     {'domain': '.qq.com', 'expiry': 1627281783, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False,
    #      'value': 'GA1.2.1285973211.1627190232'},
    #     {'domain': 'work.weixin.qq.com', 'expiry': 1627221766, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/',
    #      'secure': False, 'value': '1nru1i5'},
    #     {'domain': '.work.weixin.qq.com', 'expiry': 1658726230, 'httpOnly': False, 'name': 'wwrtx.c_gdpr',
    #      'path': '/', 'secure': False, 'value': '0'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False,
    #      'value': 'ZeugfyuG2-sRHU_E_ON5qfMOcv4r19T4uX8t7CrUxDjRKjS5iFT-vaT64TGPqr9lgvbQwDpNjmcpfO-KJP2HFgekimOrT-dyqj_MThCGv9ZXDeoZv8XfEviJAfXftT2eB3vvVSpkl-c6ZeKhxvSpJUMu55U0Pr71nEujIAlpYuAdIaNyKwBflrAq1KbcVh_ouhf-OEmWa2Z97Do5A95Ehxp4wyj3ZKI9DsGxEdoqWsO9xyumVNlkdUnlAZ-D1dmhrXHYFUQVqn6mQcLIs5mwIQ'},
    #     {'domain': '.work.weixin.qq.com', 'expiry': 1658726403, 'httpOnly': False,
    #      'name': 'Hm_lvt_9364e629af24cb52acc78b43e8c9f77d', 'path': '/', 'secure': False, 'value': '1627190232'},
    #     {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxunionid', 'path': '/',
    #      'secure': False, 'value': ''},
    #     {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'ptcz', 'path': '/', 'secure': False,
    #      'value': 'b3daeda418689164858776b604419369e620ccfb46144afc85e94440d0669be9'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': False,
    #      'value': 'direct'},
    #     {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False,
    #      'value': '1970325098462238'},
    #     {'domain': '.qq.com', 'expiry': 2147385600, 'httpOnly': False, 'name': 'pgv_pvid', 'path': '/',
    #      'secure': False, 'value': '2249441700'},
    #     {'domain': '.qq.com', 'expiry': 1651573191, 'httpOnly': False, 'name': 'eas_sid', 'path': '/',
    #      'secure': False, 'value': 'X1h6x24090O3j7k1A901C6F7p2'}]
    # for cookie in cookies:
    #     if "expiry" in cookie.keys():
    #         cookie.pop("expiry")
    #     self.driver.add_cookie(cookie)
    # self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

    def testContract(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        cookies = [
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.cs_ind', 'path': '/', 'secure': False,
             'value': ''},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False,
             'value': '37R-cfA4Dnz9OpCmz-3llIRIJbGyAvE31BqXgfpfmoFwI1Qaln6dAeczQrnPA5GDivuJ097nwi2wgElTkrbCPAXfcaraqE6MCMrKGyBAwiRSFzEPCMtQZ0uXdL4PBrZZj9aBNM-gExtzQPrEaqCJJJe6inPupOhqc84vIYtVBb9IS_ZQa_Fzu4dezzX0sbWDeFvQfOx06wdQYmf-t7PvGNxflq8YfJpPu5APLSFr6pHhQB6DSBYoFduOk08Pwj86z5tnFNa9uRhPaQgWNbNx_g'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1627221766, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/',
             'secure': True, 'value': '1nru1i5'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False,
             'value': '1688850056965497'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False,
             'value': '1688850056965497'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False,
             'value': '1970325098462238'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False,
             'value': 'jeb96G-C8QJfsp_3K0_D-jUI0oc87hqtDFuRJhIozAiFwRRI1kvyMk1efhT_8u4H'},
            {'domain': '.qq.com', 'expiry': 1627283801, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False,
             'value': 'GA1.2.1285973211.1627190232'},
            {'domain': 'work.weixin.qq.com', 'expiry': 1627221766, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/',
             'secure': False, 'value': '1nru1i5'},
            {'domain': '.qq.com', 'expiry': 1690269401, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False,
             'value': 'GA1.2.1879260742.1627190232'},
            {'domain': '.work.weixin.qq.com', 'expiry': 1629789514, 'httpOnly': False, 'name': 'wwrtx.i18n_lan',
             'path': '/', 'secure': False, 'value': 'zh'},
            {'domain': '.qq.com', 'expiry': 1652018377, 'httpOnly': False, 'name': 'LW_sid', 'path': '/',
             'secure': True, 'value': 'I10682U0q4x872d3c7R7d63425'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqopenid', 'path': '/',
             'secure': True, 'value': '961481CB923D6B51200EF0615698F47E'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': True,
             'value': '9214628972566511'}, {'domain': '.work.weixin.qq.com', 'expiry': 1658726403, 'httpOnly': False,
                                            'name': 'Hm_lvt_9364e629af24cb52acc78b43e8c9f77d', 'path': '/',
                                            'secure': True, 'value': '1627190232'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_access_token_expiresAt',
             'path': '/', 'secure': True, 'value': '1627915053'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxrefresh_token', 'path': '/',
             'secure': True, 'value': ''},
            {'domain': '.qq.com', 'expiry': 1652018377, 'httpOnly': False, 'name': 'LW_uid', 'path': '/',
             'secure': True, 'value': '31w6N2D0T4b822R3j7y7s6Q436'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxopenid', 'path': '/',
             'secure': True, 'value': ''},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqunionid', 'path': '/',
             'secure': True, 'value': ''},
            {'domain': '.work.weixin.qq.com', 'expiry': 1658726230, 'httpOnly': False, 'name': 'wwrtx.c_gdpr',
             'path': '/', 'secure': True, 'value': '0'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'tmeLoginType', 'path': '/',
             'secure': True, 'value': '2'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'wxunionid', 'path': '/',
             'secure': True, 'value': ''},
            {'domain': '.qq.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'ptcz', 'path': '/', 'secure': True,
             'value': 'b3daeda418689164858776b604419369e620ccfb46144afc85e94440d0669be9'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False,
             'value': 'a9171888'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqrefresh_token', 'path': '/',
             'secure': True, 'value': '62EBF2E9CA827F301E04CC0CEBDFFA21'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': True,
             'value': 'direct'},
            {'domain': '.qq.com', 'expiry': 2147385600, 'httpOnly': False, 'name': 'pgv_pvid', 'path': '/',
             'secure': True, 'value': '2249441700'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'psrf_qqaccess_token', 'path': '/',
             'secure': True, 'value': 'EFA3150DA9CE7CD92338DC59EEC3A38D'},
            {'domain': '.qq.com', 'expiry': 2147483646, 'httpOnly': False, 'name': 'RK', 'path': '/', 'secure': True,
             'value': 'AJaYwC27SJ'},
            {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False,
             'value': '1'},
            {'domain': '.qq.com', 'expiry': 1627915053, 'httpOnly': False, 'name': 'euin', 'path': '/', 'secure': True,
             'value': 'oK4qow-sNK-*'},
            {'domain': '.qq.com', 'expiry': 1651573191, 'httpOnly': False, 'name': 'eas_sid', 'path': '/',
             'secure': True, 'value': 'X1h6x24090O3j7k1A901C6F7p2'}]
        for cookie in cookies:
            if "expiry" in cookie.keys():
                cookie.pop("expiry")
            self.driver.add_cookie(cookie)
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        self.driver.find_element(By.CSS_SELECTOR, ".index_service_cnt_itemWrap:nth-child(2)").click()
        self.driver.find_element(By.ID, "js_upload_file_input").send_keys(r"C:\Users\86135\Desktop\mydata.xlsx")
        assert "mydata.xlsx" == self.driver.find_element(By.ID, "upload_file_name").text
        sleep(5)
