"""
第一个类Bicycle(自行车)类
run() 方法，打印骑行公里数
第二个类EBicycle继承自Bicycle
volume（电量）属性
fill_charge(vol) 充电方法
run()方法用于骑行,每骑行10km消耗电量1度 ,当电量消耗尽时调用Bicycle的run方法骑行， 通过传入的骑行里程数，显示骑行结果
"""


class Bicycle:
    def run(self, miles):
        print(f"脚踩了{miles}公里")


class EBicycle(Bicycle):
    volume = 90

    def fill_charge(self, fill_miles):
        self.volume = self.volume + fill_miles
        print(f"充电之后的电量为{self.volume}")

    def run(self, miles):
        volmiles = self.volume * 10
        if volmiles >= miles:
            print(f"电动车自动骑行了{volmiles}公里")
        else:
            print(f"电动车自动骑行了{volmiles}公里")
            super().run(miles - volmiles)


bike1 = EBicycle()
bike1.run(1000)
