from exercise1.heroClass import Timo, Jinx


class heroFactory:
    @staticmethod
    def create_hero(hero):
        if hero == "timo":
            return Timo()
        elif hero == "jinx":
            return Jinx()
        else:
            raise Exception("该英雄找不到")


# 实例化一个timo对象
if __name__ == '__main__':
    timo2 = heroFactory.create_hero("timo")
    jinx2 = heroFactory.create_hero("jinx")
    timo2.fight(jinx2.hp, jinx2.power)
