class hero:
    # 实例的血量
    hp = 0
    # 实例的攻击力
    power = 0

    def fight(self, enemy_hp, enemy_power):
        """
        :param enemy_hp: 敌人的血量
        :param enemy_power: 敌人的攻击力
        :return:
        """
        # 英雄剩余的血量
        self.hp = self.hp - enemy_power
        # 敌人的血量
        enemy_hp = enemy_hp - self.power
        if self.hp > enemy_hp:
            print("英雄获胜")
        elif self.hp < enemy_hp:
            print("敌人获胜")
        else:
            print("打平了")


class Timo(hero):
    hp = 1900
    power = 1000


class Jinx(hero):
    hp = 2200
    power = 1000

if __name__ == '__main__':
    timo = Timo()
    jinx = Jinx()
    timo.fight(jinx.hp, jinx.power)
