from api_test.api.main import Main


class TestCase:
    def setup(self):
        self.main = Main()

    def tear_down(self):
        pass

    def test_getRequest(self):
        params = {"a": 30, "b": 40}
        r = self.main.test_get(params)
        assert r.status_code == 200
        print(r.json().get("args")==params)

    def test_postRequest(self):
        params = {"a": 30, "b": 40}
        data = {"d":50}
        r = self.main.test_post(params,data)
        assert r.status_code == 200

    def test_deleteReuest(self):
        params = {"a": 30, "b": 40}
        r = self.main.test_delete(params)
        assert r.status_code == 200
