import requests


class Main:
    # 测试get请求
    def test_get(self, params):
        r = requests.get("https://httpbin.ceshiren.com/get", params=params)
        return r

    def test_post(self,params,data):
        r = requests.post("https://httpbin.ceshiren.com/post",params=params,data=data)
        return r

    def test_delete(self,params):
        r = requests.delete("https://httpbin.ceshiren.com/delete",params = params)
        return r
