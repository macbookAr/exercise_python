###
# 定义一个计算器的类
#
#
#
# ###
# 定义一个测试 类
class Calculator:
    # 定义计算器的加法：
    def test_add(self, b, c):
        a = b + c
        return a

    # 定义计算器的减法
    def test_sub(self, b, c):
        a = b - c
        return a

    # 定义乘法
    def test_mul(self, b, c):
        a = b * c
        return a

    # 定义除法
    def test_div(self, b, c):
        a = b / c
        return a
