from test_appium.test_po2.page.app import App


class TestContact:
    def setup(self):
        # 完成app的启动，driver的初始化
        self.app = App()
        self.main = self.app.start()

    def teardown(self):
        # 关闭 app
        self.app.stop()

    def test_addcontact(self):
        result = self.main.goto_main().goto_addressList().goto_addmemberPage().click_addmember_menual().edit_member().get_tip()
        assert result == '添加成功'