from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.test_po2.page.addmember_page import AddmemberPage
from test_appium.test_po2.page.base_page import BasePage


class AddresslistPage(BasePage):
    clickAdd_element = (MobileBy.XPATH, "//*[@text='添加成员']")
    def goto_addmemberPage(self):
        #点击添加成员
        self.find_and_click(*self.clickAdd_element)
        return AddmemberPage(self.driver)