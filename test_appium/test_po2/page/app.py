#存放app相关的操作，打开，重启，关闭
from appium.webdriver.common.mobileby import MobileBy

from test_appium.test_po1.page.main_page import MainPage
from appium import webdriver


class App:
    def start(self):
        desire_caps = {}
        desire_caps['platformName'] = 'Android'
        desire_caps['platformVersion'] = '6.0'
        desire_caps['deviceName'] = '127.0.0.1:7555'
        desire_caps['appPackage'] = 'com.tencent.wework'
        desire_caps['appActivity'] = '.launch.WwMainActivity'
        desire_caps['noReset'] = 'true'
        # desire_caps['dontStopAppOnReset'] = 'true'
        desire_caps['unicodeKeyBoard'] = 'true'
        desire_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_caps)
        self.driver.implicitly_wait(10)
        return self

    def restart(self):
        pass

    def stop(self):
        self.driver.quit()

    def goto_main(self):
        #入口
        return MainPage(self.driver)