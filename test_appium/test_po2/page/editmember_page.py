from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from faker import Faker



class EditMemberPage(object):
    textName_element = (MobileBy.XPATH, "//*[contains(@text,'姓名')]/../android.widget.EditText")
    phonenumber_element = (MobileBy.XPATH, "//*[contains(@text,'手机')]/..//*[@text='必填']")
    save_element = (MobileBy.XPATH, "//*[@text='保存并继续添加']")
    def edit_member(self):
        self.fake = Faker('zh_CN')
        name = self.fake.name()
        phoneNum = self.fake.phone_number()
        from test_appium.test_po2.page.addmember_page import AddmemberPage
        self.find_and_send(*self.textName_element, name)
        self.find_and_send(*self.phonenumber_element, phoneNum)
        self.find_and_click(*self.save_element)
        return AddmemberPage(self.driver)


