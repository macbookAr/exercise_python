from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.test_po3.page.base_page import BasePage
from test_appium.test_po3.page.editmember_page import EditMemberPage


class AddmemberPage(BasePage):
    addmemberMenual_click = (MobileBy.XPATH, "//*[@text='手动输入添加']")
    def click_addmember_menual(self):
        self.find_and_click(*self.addmemberMenual_click)
        return EditMemberPage(self.driver)

    def get_tip(self):
        result = self.find(MobileBy.XPATH, "//*[@class='android.widget.Toast']").get_attribute("text")
        return result