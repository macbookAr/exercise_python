#初始化driver
from appium.webdriver.webdriver import WebDriver


class BasePage:
    def __init__(self,driver:WebDriver=None):
        self.driver = driver

    def confirmbtn_elementfind(self, by,value):
        return self.driver.find_element(by,value)

    def find(self, by,value):
        return self.driver.find_element(by,value)

    def find_and_click(self,by,value):
        self.find(by,value).click()

    def find_and_send(self,by,value,text):
        self.find(by,value).send_keys(text)

    def back(self,num=3):
        for i in range(0,num):
            self.driver.back()

