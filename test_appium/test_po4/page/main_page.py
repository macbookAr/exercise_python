from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.test_po4.page.addresslist_page import AddresslistPage
from test_appium.test_po4.page.base_page import BasePage


class MainPage(BasePage):
    addressList_element = (MobileBy.XPATH,"//*[@text='通讯录']")

    def goto_addressList(self):
        self.find_and_click(*self.addressList_element)
        return AddresslistPage(self.driver)

