#初始化driver
import logging

from appium.webdriver.webdriver import WebDriver

logger = logging.getLogger()
class BasePage:
    def __init__(self,driver:WebDriver=None):
        self.driver = driver

    def confirmbtn_elementfind(self, by,value):
        return self.driver.find_element(by,value)

    def find(self, by,value):
        logger.info("find")
        logger.info(by)
        logger.info(value)
        return self.driver.find_element(by,value)

    def find_and_click(self,by,value):
        logger.info("find_and_click")
        self.find(by,value).click()

    def find_and_send(self,by,value,text):
        logger.info("find_and_send")
        self.find(by,value).send_keys(text)

    def back(self,num=3):
        logger.info("back")
        for i in range(0,num):
            self.driver.back()

