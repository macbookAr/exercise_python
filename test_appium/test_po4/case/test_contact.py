from test_appium.test_po4.page.app import App
import  sys
print(sys.path)
sys.path.append("../..")
class TestContact:
    def setup(self):
        # 完成app的启动，driver的初始化
        self.app = App()
        self.main = self.app.start()

    def teardown(self):
        # 关闭 app
        self.app.back()

    def teardownclass(self):
        self.app.stop()

    def test_addcontact(self):
        result = self.main.goto_main().goto_addressList().goto_addmemberPage().click_addmember_menual().edit_member().get_tip()
        assert result == '添加成功'

    def test_delcontact(self):
        result = self.main.goto_main().goto_addressList().goto_contactDetail().delContact()
        assert result
