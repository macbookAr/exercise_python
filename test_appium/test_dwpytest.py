from time import sleep

import pytest
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction


class TestDw():
    def setup(self):
        desire_caps = {}
        desire_caps['platformName'] = 'Android'
        desire_caps['platformVersion'] = '6.0'
        desire_caps['deviceName'] = '127.0.0.1:7555'
        desire_caps['appPackage'] = 'com.xueqiu.android'
        desire_caps['appActivity'] = '.view.WelcomeActivityAlias'
        desire_caps['noReset'] = 'true'
        # desire_caps['dontStopAppOnReset'] = 'true'
        desire_caps['unicodeKeyBoard'] = 'true'
        desire_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        # self.driver.back()
        # self.driver.back()
        self.driver.quit()

    def test_search(self):
        print("搜索测试案例")
        """
        1.打开雪球app
        2.点击搜索输入框
        3.向搜索输入框里面输入“阿里巴巴”
        4.在搜索结果里面选择“阿里巴巴”，然后进行点击
        5.获取这只上阿里巴巴的股价，并判断这只股价的价格》200
        """
        self.driver.find_element_by_id("com.xueqiu.android:id/home_search").click()
        self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")  # def set_up(self):

    def test_attr(self):
        element = self.driver.find_element_by_id("com.xueqiu.android:id/home_search")
        search_enabled = element.is_enabled()
        print(element.text)
        print(element.location)
        print(element.size)
        if search_enabled == True:
            element.click()
            self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").send_keys("alibaba")
            alibaba_element = self.driver.find_element_by_xpath( "//*[@resource-id='com.xueqiu.android:id/name' and @text='阿里巴巴']")
            element_displayed = alibaba_element.get_attribute("displayed")
            if element_displayed == 'true':
                print("搜索成功")
            else:
                print("搜索失败")

    def test_touchAction(self):
        action = TouchAction(self.driver)
        window_rect = self.driver.get_window_rect()
        width = window_rect['width']
        height = window_rect['height']
        x1 = int(width/2)
        y_start = int(height * 4/5)
        y_stop = int(height * 1/5)
        action.press(x=x1,y=y_start).wait(200).move_to(x=x1,y=y_start).release().perform()



if __name__ == '__main__':
    pytest.main()

#
