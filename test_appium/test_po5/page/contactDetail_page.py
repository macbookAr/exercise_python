from time import sleep

from appium.webdriver.common.touch_action import TouchAction

from test_appium.test_po4.page.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

class ContactDetailPage(BasePage):

    setting_element = (MobileBy.XPATH, "//*[@text='编辑成员']")
    delbtn_element = (MobileBy.XPATH, "//*[@text='删除成员']")
    confirmbtn_element = (MobileBy.XPATH, "//*[@text='确定']")

    def delContact(self):
        from test_appium.test_po3.page.addresslist_page import AddresslistPage
        self.driver.find_element_by_id("com.tencent.wework:id/isv").click()
        self.find_and_click(*self.setting_element)
        sleep(3)
        TouchAction(self.driver).press(x=371, y=1088).wait(200).move_to(x=371, y=803).release().perform()
        # action1 = TouchAction(self.driver)
        delete_btn = self.find(*self.delbtn_element)
        TouchAction(self.driver).tap(delete_btn).perform()
        confirm_btn = self.find(*self.confirmbtn_element)
        TouchAction(self.driver).tap(confirm_btn).perform()
        sleep(5)
        print(self.driver.page_source)

        return AddresslistPage(self.driver)



