from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from test_appium.test_po4.page.addmember_page import AddmemberPage
from test_appium.test_po4.page.base_page import BasePage
from test_appium.test_po4.page.contactDetail_page import ContactDetailPage


class AddresslistPage(BasePage):
    clickAdd_element = (MobileBy.XPATH, "//*[@text='添加成员']")
    contactDetail_element = (MobileBy.XPATH, "//*[@text='企业通讯录']/../android.widget.RelativeLayout")
    def goto_addmemberPage(self):
        #点击添加成员
        self.find_and_click(*self.clickAdd_element)
        return AddmemberPage(self.driver)

    def goto_contactDetail(self):
        self.find_and_click(*self.contactDetail_element)
        return ContactDetailPage(self.driver)

