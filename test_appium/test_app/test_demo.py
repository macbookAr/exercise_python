from appium.webdriver import webdriver

desire_caps = {}
desire_caps['platformName'] = 'Android'
desire_caps['platformVersion'] = '6.0'
desire_caps['deviceName'] = '127.0.0.1:7555'
desire_caps['appPackage'] = 'com.tencent.wework'
desire_caps['appActivity'] = '.launch.WwMainActivity'
desire_caps['noReset'] = 'true'
# desire_caps['dontStopAppOnReset'] = 'true'
desire_caps['unicodeKeyBoard'] = 'true'
desire_caps['resetKeyBoard'] = 'true'
driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_caps)
driver.implicitly_wait(10)