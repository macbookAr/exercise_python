from time import sleep

import pytest
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
from faker import Faker


class TestContact:
    def setup_class(self):
        self.fake = Faker('zh_CN')
    def setup(self):
        desire_caps = {}
        desire_caps['platformName'] = 'Android'
        desire_caps['platformVersion'] = '6.0'
        desire_caps['deviceName'] = '127.0.0.1:7555'
        desire_caps['appPackage'] = 'com.tencent.wework'
        desire_caps['appActivity'] = '.launch.WwMainActivity'
        desire_caps['noReset'] = 'true'
        # desire_caps['dontStopAppOnReset'] = 'true'
        desire_caps['unicodeKeyBoard'] = 'true'
        desire_caps['resetKeyBoard'] = 'true'
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_caps)
        self.driver.implicitly_wait(10)

    def teardown(self):
        self.driver.quit()


    def test_addcontact(self):
        name = self.fake.name()
        phoneNum = self.fake.phone_number()
        clickContact = self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']")
        if clickContact.is_enabled():
            clickContact.click()
            clickAdd = self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成员']")
            if clickAdd.is_enabled():
                clickAdd.click()
                self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
                self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'姓名')]/../android.widget.EditText").send_keys(name)
                self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'手机')]/..//*[@text='必填']").send_keys(phoneNum)
                self.driver.find_element(MobileBy.XPATH,"//*[@text='保存并继续添加']").click()
                result = self.driver.find_element(MobileBy.XPATH,"//*[@class='android.widget.Toast']").get_attribute("text")
                assert  "添加成功" == result
                sleep(2)
                print(self.driver.page_source)


    def test_delcontact(self):
        clickContact = self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']")
        if clickContact.is_enabled():
            clickContact.click()
            # 进入到单个通讯录的详情页
            clickusername = self.driver.find_element(MobileBy.XPATH,"//*[@text='企业通讯录']/../android.widget.RelativeLayout")
            if clickusername.is_enabled():
                clickusername.click()
                self.driver.find_element_by_id("com.tencent.wework:id/isv").click()
                self.driver.find_element(MobileBy.XPATH,"//*[@text='编辑成员']").click()
                sleep(3)
                # action = TouchAction(self.driver)
                # window_rect = self.driver.get_window_rect()
                # width = window_rect['width']
                # height = window_rect['height']
                # x1 = int(width / 2)
                # y_start = int(height * 4 / 5)
                # y_stop = int(height * 1 / 5)
                TouchAction(self.driver).press(x=371, y=1088).wait(200).move_to(x=371, y=803).release().perform()
                # action1 = TouchAction(self.driver)
                delete_btn = self.driver.find_element(MobileBy.XPATH, "//*[@text='删除成员']")
                TouchAction(self.driver).tap(delete_btn).perform()
                confirm_btn = self.driver.find_element(MobileBy.XPATH, "//*[@text='确定']")
                TouchAction(self.driver).tap(confirm_btn).perform()
                sleep(5)
                print(self.driver.page_source)

            else:
                print("未找到")
        else:
            print("未找到")

if __name__ == '__main__':
    pytest.main()
