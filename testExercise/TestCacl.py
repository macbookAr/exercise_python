###
# 用来编写测试案例
#
#
# ###
# from exercise2.cacl import Calculator
import logging

import allure
import pytest
import yaml

# 获取yaml文件的数据
with open("./datas/cacl.yaml") as f:
    datas = yaml.safe_load(f)['datas']
    # 获取加法的数据
    add_datas = datas['add_data']
    # 获取减法的数据
    sub_datas = datas['sub_data']
    # 获取乘法的数据
    mul_datas = datas['mul_data']
    # 获取除法的数据
    div_datas = datas['div_data']
    ids = datas['myids']

logging.basicConfig(level=logging.INFO)
# 定义一个实例
logger = logging.getLogger()

@allure.feature("测试计算器")
class TestExercise:

    # # 定义设置为前置
    # def setup_class(self):
    #     self.cacl = Calculator()
    #     print("开始计算")
    #
    # def teardown_class(self):
    #     print("结束计算")

    @allure.story("测试相加")
    @pytest.mark.parametrize(
        "a, b, expect", add_datas, ids=ids
    )
    # 验证加法的测试方法
    def test_add(self, getCacl, a, b, expect):
        # cacl = Calculator()
        with allure.step("计算两个数相加"):
            result = getCacl.test_add(a, b)
        if isinstance(result, float):
            result = round(result, 2)
        logger.info("参数a的值是："+str(a)+"，参数b的值是："+str(b)+"结果是："+str(result))
        assert result == expect


    @allure.story("测试相减")
    @pytest.mark.parametrize(
        "a, b, expect", sub_datas, ids=ids
    )
    # 验证减法的测试方法
    def test_sub(self, getCacl, a, b, expect):
        # cacl = Calculator()
        with allure.step("计算两个数相减"):
            result = getCacl.test_sub(a, b)
        if isinstance(result, float):
            result = round(result, 2)
        logger.info("参数a的值是："+str(a)+"，参数b的值是："+str(b)+"结果是："+str(result))
        assert result == expect

    @allure.story("测试相乘")
    @pytest.mark.parametrize(
        "a, b, expect", mul_datas, ids=ids
    )
    # # 验证乘法的测试方法
    def test_mul(self, getCacl, a, b, expect):
        # cacl = Calculator()
        with allure.step("计算两个数相乘"):
            result = getCacl.test_mul(a, b)
        if isinstance(result, float):
            result = round(result, 2)
        print()
        logger.info("参数a的值是："+str(a)+"，参数b的值是："+str(b)+"结果是："+str(result))
        assert result == expect

    @allure.story("测试相除")
    @pytest.mark.parametrize(
        "a, b, expect", div_datas, ids=ids
    )
    # # 验证除法的测试方法
    def test_div(self, getCacl, a, b, expect):
        # cacl = Calculator()
        with allure.step("计算两个数相除"):
            result = getCacl.test_div(a, b)
        if isinstance(result, float):
            result = round(result, 2)
        logger.info("参数a的值是："+str(a)+"，参数b的值是："+str(b)+"结果是："+str(result))
        assert result == expect
