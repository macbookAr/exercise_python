import pytest

from exercise2.cacl import Calculator


@pytest.fixture(scope="class")
def getCacl():
    print("开始计算")
    cacl = Calculator()
    yield cacl
    print("结束计算")
