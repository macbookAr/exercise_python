import json

import mitmproxy
from mitmproxy import http, ctx
from mitmproxy.tools._main import mitmdump


class Events:
    def http_connect(self, flow: mitmproxy.http.HTTPFlow):
        """
            An HTTP CONNECT request was received. Setting a non 2xx response on
            the flow will return the response to the client abort the
            connection. CONNECT requests and responses do not generate the usual
            HTTP handler events. CONNECT requests are only valid in regular and
            upstream proxy modes.
        """

    def requestheaders(self, flow: mitmproxy.http.HTTPFlow):
        """
            HTTP request headers were successfully read. At this point, the body
            is empty.
        """

    def request(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP request has been read.
            在request请求中实现maplocal操作
        """
        '''https://stock.xueqiu.com/v5/stock/batch/quote.json?_t=1NETEASE55d7e8578966f744d5452096654f6f42.4558458945.1624970447480.1624971747547&_s=250a2c&x=0.51&symbol=SZ300339%2CMF%2CSH600702%2CSZ300059%2CSH601919%2CBABA%2CSH601318&extend=detail'''
        # if "quote.json" in flow.request.pretty_url:
        #     with open("quote.json", encoding="utf-8") as f:
        #         flow.response = http.HTTPResponse.make(
        #             200,
        #             f.read(),
        #             {"content-type": "application/json"}
        #         )
        pass

    def responseheaders(self, flow: mitmproxy.http.HTTPFlow):
        """
            HTTP response headers were successfully read. At this point, the body
            is empty.
        """

    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
            实现rewrite
        """
        if "quote.json" in flow.request.pretty_url and "x=" in flow.request.pretty_url:
            # data = json.loads(flow.response.content)
            # ctx.log.info(data)
            #打印获取的请求体
            ctx.log.info(str(flow.response.content))
            data = json.loads(flow.response.content)
            with open("data.json", "w", encoding="utf-8") as f:
                f.write(str(data))
            data['data']['items'][0]['quote']["name"] = "zmmtest"
            data['data']['items'][1]['quote']["name"] = "zmmtest"
            data['data']['items'][1]['quote']["percent"] = "0.00"
            data['data']['items'][2]['quote']["current"] = "-0.01"
            # data['data']['items'][3]['quote']["percent"] = "0.01"
            # data['data']['items'][4]['quote']["percent"] = "99.99"
            flow.response.text = json.dumps(data)

    def error(self, flow: mitmproxy.http.HTTPFlow):
        """
            An HTTP error has occurred, e.g. invalid server responses, or
            interrupted connections. This is distinct from a valid server HTTP
            error response, which is simply a response with an HTTP error code.
        """


addons = [
    Events()
]

if __name__ == '__main__':
    mitmdump(['-s', __file__, '-p', "8080"])
